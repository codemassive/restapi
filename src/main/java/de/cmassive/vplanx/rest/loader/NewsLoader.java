package de.cmassive.vplanx.rest.loader;

import de.cmassive.vplanx.rest.model.news.News;

import java.util.ArrayList;

public interface NewsLoader {

    ArrayList<News> getNewsBySchool(int schoolId);

}
