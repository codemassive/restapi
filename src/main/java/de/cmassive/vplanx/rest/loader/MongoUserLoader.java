package de.cmassive.vplanx.rest.loader;

import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;

public class MongoUserLoader implements UserLoader {

    @Override
    public VpxUser getUserById(String id) {
        Document document = RestApplication.getMongoConnection().getAccountCollection().find(new BasicDBObject("accountUID", id)).first();
        if(document == null) {
            return null;
        }

        return DocumentConverter.userFromDocument(document);
    }
}
