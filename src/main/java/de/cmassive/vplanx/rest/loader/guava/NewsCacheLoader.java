package de.cmassive.vplanx.rest.loader.guava;

import com.google.common.cache.CacheLoader;
import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.rest.except.SchoolNotFoundException;
import de.cmassive.vplanx.rest.loader.DocumentConverter;
import de.cmassive.vplanx.rest.model.news.News;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class NewsCacheLoader extends CacheLoader<Integer, ArrayList<News>> {


    @Override
    public ArrayList<News> load(Integer schoolId) throws Exception {
        Document schoolNewsDoc = RestApplication.getMongoConnection().getNewsCollection().find(new BasicDBObject("schoolId", schoolId)).first();
        if(schoolNewsDoc == null) {
            throw new SchoolNotFoundException();
        }

        ArrayList<News> newsArrayList = new ArrayList<>();

        if(schoolNewsDoc.containsKey("news")) {
            for(Document newsDoc : (List<Document>) schoolNewsDoc.get("news")) {
                newsArrayList.add(DocumentConverter.newsFromDocument(newsDoc));
            }
        }

        return newsArrayList;
    }
}
