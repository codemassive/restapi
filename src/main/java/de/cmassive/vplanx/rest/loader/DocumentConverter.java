package de.cmassive.vplanx.rest.loader;

import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.Endpoint;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import de.cmassive.vplanx.rest.model.feature.VpxFeature;
import de.cmassive.vplanx.rest.model.feature.VpxFlag;
import de.cmassive.vplanx.rest.model.news.News;
import de.cmassive.vplanx.rest.model.school.VpxAddress;
import de.cmassive.vplanx.rest.model.school.VpxContact;
import de.cmassive.vplanx.rest.model.school.VpxInfo;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitution;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitutionDay;
import de.cmassive.vplanx.rest.model.user.VpxSubscription;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import de.cmassive.vplanx.rest.rest.RestCommon;
import org.bson.Document;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DocumentConverter {

    public static VpxInfo infoFromDocument(Document document) {
        VpxInfo info = new VpxInfo();
        info.setName(document.getString("name"));
        info.setShortName(document.getString("short"));
        info.setPictureUrl(document.getString("picture"));
        return info;
    }

    public static VpxAddress addressFromDocument(Document document) {
        VpxAddress address = new VpxAddress();
        address.setCity(document.getString("city"));
        address.setCountry(document.getString("country"));
        address.setHnum(document.getString("hnum"));
        address.setStreet(document.getString("street"));
        address.setZipcode(document.getString("zipcode"));
        return address;
    }

    public static VpxContact contactFromDocument(Document document) {
        VpxContact contact = new VpxContact();
        contact.setEmail(document.getString(document.getString("email")));
        contact.setFax(document.getString(document.getString("fax")));
        contact.setPhone(document.getString(document.getString("phone")));
        return contact;
    }

    public static Set<VpxFeature> featuresFromDocument(Document document) {
        HashSet<VpxFeature> features = new HashSet<>();

        for(Document ftDoc : (List<Document>) document.get("features")) {
            features.add(featureFromDocument(ftDoc));
        }

        return features;
    }

    public static AuthContainer authFromDocument(Document document) {
        if(!document.containsKey("type")) return null;

        AuthContainer container = new AuthContainer();
        container.setAuthType(document.getString("type"));
        container.setExpectedResult(document.getString("expected"));

        if(document.containsKey("data")) {
            Document authDoc = (Document) document.get("data");

            if(container.getAuthType().equalsIgnoreCase("auth-static")) {
                container.setAuthInfo(new UserPasswordAuth(authDoc.getString("username"), authDoc.getString("password")));
            }
        }

        return container;
    }

    public static VpxFeature featureFromDocument(Document document) {
        VpxFeature feature = new VpxFeature();
        feature.setName(document.getString("name"));

        if(document.containsKey("urls")) {
            for(String s : (List<String>) document.get("urls")) {
                try {
                    feature.getEndpoints().add(new Endpoint(new URL(s)));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        if(document.containsKey("auth")) {
            feature.setAuthContainer(authFromDocument((Document) document.get("auth")));
        }

        if(document.containsKey("flags")) {
            List<Document> flagArray = (List<Document>) document.get("flags");
            for(Document flagDoc : flagArray) {
                feature.getFlags().add(new VpxFlag(flagDoc.getString("name"), flagDoc.getString("value")));
            }
        }

        //TODO: Additional Data

        return feature;
    }

    public static VpxUser userFromDocument(Document document) {
        int accountId = document.getInteger("accountId");
        String accountExpId = document.getString("accountUID");

        VpxUser user = new VpxUser(accountId, accountExpId);
        if(document.containsKey("subscriptions")) {
            List<Document> documentList = (List<Document>) document.get("subscriptions");
            for(Document subDoc : documentList) {
                VpxSubscription subscription = DocumentConverter.subscriptionFromDocument(subDoc, user.getAccountId());
                if(!RestCommon.hasUserValidSubscription(
                        RestApplication.getSchoolLoader().getSchoolById(subscription.getSchoolId()),
                        subscription
                )) {
                    RestCommon.unsubscribeUserFromFeature(subscription);
                } else {
                    user.addSubscription(subscription);
                }
            }
        }

        return user;
    }

    public static VpxSubscription subscriptionFromDocument(Document document, int accountId) {
        String ftName = document.getString("feature");
        VpxSubscription subscription = new VpxSubscription(document.getInteger("subscriptionId"), document.getInteger("schoolId"), accountId, ftName);

        if(document.containsKey("auth")) {
            subscription.setAuthContainer(authFromDocument((Document) document.get("auth")));
        }

        return subscription;
    }

    public static VpxSubstitutionDay substitutionDayFromDocument(Document document) {
        VpxSubstitutionDay day = new VpxSubstitutionDay(LocalDate.parse(document.getString("date"), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        day.setId(document.getInteger("dayId"));
        day.setMessageOfTheDay(document.getString("motd"));
        day.setAccountId(document.getInteger("accountId", -1));

        for(Document subDoc : (List<Document>) document.get("subs")) {
            day.addSubstitution(substitutionFromDocument(subDoc));
        }

        return day;
    }

    public static VpxSubstitution substitutionFromDocument(Document subDoc) {
        LocalDateTime subDate = LocalDateTime.parse(subDoc.getString("date"), DateTimeFormatter.ofPattern("dd.MM.yyyy'T'HH:mm"));

        VpxSubstitution sub = new VpxSubstitution();
        sub.setId(subDoc.getInteger("subId"));
        sub.setDateTime(subDate);

        sub.setNewSubject(subDoc.getString("newSubject"));
        sub.setOldSubject(subDoc.getString("oldSubject"));
        sub.setNewRoom(subDoc.getString("newRoom"));
        sub.setOldRoom(subDoc.getString("oldRoom"));
        sub.setDescription(subDoc.getString("description"));
        sub.setOldTeacher(listToSet((List<String>) subDoc.get("oldTeacher")));
        sub.setNewTeacher(listToSet((List<String>) subDoc.get("newTeacher")));
        sub.setLessonString(subDoc.getString("lessonName"));
        sub.setClasses(listToSet((List<String>) subDoc.get("classes")));

        return sub;
    }

    public static News newsFromDocument(Document document) {
        LocalDate newDate = LocalDate.parse(document.getString("published"), DateTimeFormatter.ofPattern("dd.MM.yyyy"));

        News n = new News();
        n.setPublishedDate(newDate);
        n.setAuthor(document.getString("author"));
        n.setContent(document.getString("content"));
        n.setMainImageUrl(document.getString("image"));
        n.setNewsId(document.getInteger("newsId"));
        n.setHeadline(document.getString("headline"));
        return n;
    }

    public static<T> Set<T> listToSet(List<T> list) {
        Set<T> set = new HashSet<>();
        for(T t : list) {
            set.add(t);
        }
        return set;
    }
}
