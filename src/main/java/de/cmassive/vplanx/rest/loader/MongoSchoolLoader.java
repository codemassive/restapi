package de.cmassive.vplanx.rest.loader;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import com.mongodb.client.FindIterable;
import de.cmassive.vplanx.rest.loader.guava.SchoolCacheLoader;
import de.cmassive.vplanx.rest.model.school.VpxSchool;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class MongoSchoolLoader implements SchoolLoader {

    private LoadingCache<Integer, VpxSchool> schoolLoadingCache;

    public MongoSchoolLoader() {
        this.schoolLoadingCache = CacheBuilder.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).build(new SchoolCacheLoader());
    }

    @Override
    public VpxSchool getSchoolById(int id) {
        return schoolLoadingCache.getUnchecked(id);
    }

    @Override
    public Set<VpxSchool> getAllSchools() {
        final Set<VpxSchool> schoolList = new HashSet<>();

        FindIterable<Document> findIterable = RestApplication.getMongoConnection().getSchoolCollection().find();
        findIterable.forEach(new Consumer<Document>() {
            @Override
            public void accept(Document document) {
                int schoolId = document.getInteger("schoolId");
                schoolList.add(schoolLoadingCache.getUnchecked(schoolId));
            }
        });

        return schoolList;
    }
}
