package de.cmassive.vplanx.rest.loader;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import de.cmassive.vplanx.rest.loader.guava.NewsCacheLoader;
import de.cmassive.vplanx.rest.model.news.News;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MongoNewsLoader implements NewsLoader {

    private LoadingCache<Integer, ArrayList<News>> newsLoadingCache;

    public MongoNewsLoader() {
        this.newsLoadingCache = CacheBuilder.newBuilder().expireAfterWrite(20, TimeUnit.MINUTES).build(new NewsCacheLoader());
    }

    @Override
    public ArrayList<News> getNewsBySchool(int schoolId) {
        return newsLoadingCache.getUnchecked(schoolId);
    }
}
