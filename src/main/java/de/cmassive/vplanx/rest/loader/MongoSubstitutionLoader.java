package de.cmassive.vplanx.rest.loader;

import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.lib.substitution.SubstitutionDay;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitution;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitutionDay;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class MongoSubstitutionLoader implements SubstitutionLoader {


    @Override
    public Set<VpxSubstitution> getSubById(int subId) {
        return null;
    }

    @Override
    public Set<VpxSubstitution> getSubsBySchool(int schoolId) {
        return null;
    }

    @Override
    public Set<VpxSubstitutionDay> getSubDaysBySchoolUser(int schoolId, VpxUser user) {
        Document document = RestApplication.getMongoConnection().getSubstitutionCollection().find(new BasicDBObject("schoolId", schoolId)).first();
        Set<VpxSubstitutionDay> substitutionDaySet = new HashSet<>();
        List<Document> documentList;

        if(document.containsKey("userDays") && ((List<Document>) document.get("userDays")).size() > 0) {
            documentList = (List<Document>) document.get("userDays");
        } else {
            documentList = (List<Document>) document.get("days");
        }

        for(Document dayDoc : documentList) {
            if(dayDoc.containsKey("accountId")) {
                if(dayDoc.getInteger("accountId") != user.getAccountId()) {
                    continue;
                }
            }

            substitutionDaySet.add(DocumentConverter.substitutionDayFromDocument(dayDoc));
        }

        return substitutionDaySet;
    }

    @Override
    public Set<VpxSubstitutionDay> getSubDaysBySchoolUser(int schoolId, VpxUser user, String schoolClass) {
        Set<VpxSubstitutionDay> days = getSubDaysBySchoolUser(schoolId, user);
        Iterator<VpxSubstitutionDay> dayIterator = days.iterator();
        while(dayIterator.hasNext()) {
            VpxSubstitutionDay day = dayIterator.next();

            Iterator<VpxSubstitution> substitutionIterator = day.getSubstitutionList().iterator();
            while(substitutionIterator.hasNext()) {
                if(!substitutionIterator.next().getClasses().contains(schoolClass)){
                    substitutionIterator.remove();
                }
            }

            if(day.getSubstitutionList().isEmpty()) {
                dayIterator.remove();
            }
        }

        return days;
    }


}
