package de.cmassive.vplanx.rest.loader;

import de.cmassive.vplanx.rest.model.substitution.VpxSubstitution;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitutionDay;
import de.cmassive.vplanx.rest.model.user.VpxUser;

import java.util.Set;

public interface SubstitutionLoader {

    public Set<VpxSubstitution> getSubById(int subId);
    public Set<VpxSubstitution> getSubsBySchool(int schoolId);
    public Set<VpxSubstitutionDay> getSubDaysBySchoolUser(int schoolId, VpxUser user);
    public Set<VpxSubstitutionDay> getSubDaysBySchoolUser(int schoolId, VpxUser user, String schoolClass);

}
