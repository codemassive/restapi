package de.cmassive.vplanx.rest.loader;


import de.cmassive.vplanx.rest.model.school.VpxSchool;

import java.util.Set;

public interface SchoolLoader {

    public VpxSchool getSchoolById(int id);
    public Set<VpxSchool> getAllSchools();
}