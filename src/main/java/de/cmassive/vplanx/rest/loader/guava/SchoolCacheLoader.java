package de.cmassive.vplanx.rest.loader.guava;

import com.google.common.cache.CacheLoader;
import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.rest.except.SchoolNotFoundException;
import de.cmassive.vplanx.rest.loader.DocumentConverter;
import de.cmassive.vplanx.rest.model.school.VpxSchool;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;

import java.util.List;

public class SchoolCacheLoader extends CacheLoader<Integer, VpxSchool> {

    @Override
    public VpxSchool load(Integer integer) throws Exception {
        BasicDBObject object = new BasicDBObject("schoolId", integer);

        Document basic = RestApplication.getMongoConnection().getSchoolCollection().find(object).first();
        if(basic == null) {
            throw new SchoolNotFoundException();
        }

        VpxSchool school = new VpxSchool();
        school.setSchoolClasses((List<String>) basic.get("classes"));
        school.setSchoolId(integer);
        school.setSchoolAddress(DocumentConverter.addressFromDocument((Document) basic.get("address")));
        school.setSchoolContact(DocumentConverter.contactFromDocument((Document) basic.get("contact")));
        school.setSchoolInfo(DocumentConverter.infoFromDocument((Document) basic.get("info")));

        Document ftDoc = RestApplication.getMongoConnection().getSchoolFeatureCollection().find(object).first();
        if(ftDoc != null) {
            school.setFeatures(DocumentConverter.featuresFromDocument(ftDoc));
        }

        return school;
    }

}
