package de.cmassive.vplanx.rest.loader;

import de.cmassive.vplanx.rest.model.user.VpxUser;

public interface UserLoader {

    public VpxUser getUserById(String id);

}
