package de.cmassive.vplanx.rest.rest;

import de.cmassive.vplanx.rest.loader.*;
import de.cmassive.vplanx.rest.model.connection.VpxConnection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("de.cmassive.vplanx.rest.controller")
public class RestApplication {

	private static VpxConnection CONNECTION;
	private static MongoSchoolLoader SCHOOL_LOADER;
	private static MongoUserLoader USER_LOADER;
	private static MongoSubstitutionLoader SUB_LOADER;
	private static MongoNewsLoader NEWS_LOADER;

	public static final int API_LEVEL = 1;
	public static final String API_NAME = "Planomy REST-API v" + API_LEVEL;
	public static final String API_DEVELOPERS = "Johannes Quast";
	public static final String API_COMPANY = "codeMASSIVE";

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(RestApplication.class, args);
		CONNECTION = new VpxConnection("localhost", 27017);

		SCHOOL_LOADER = new MongoSchoolLoader();
		USER_LOADER = new MongoUserLoader();
		SUB_LOADER = new MongoSubstitutionLoader();
		NEWS_LOADER = new MongoNewsLoader();
	}

	public static VpxConnection getMongoConnection() {
		return CONNECTION;
	}
	public static UserLoader getUserLoader() { return USER_LOADER; }
	public static SchoolLoader getSchoolLoader() { return SCHOOL_LOADER; }
	public static SubstitutionLoader getSubLoader() { return SUB_LOADER; }
	public static NewsLoader getNewsLoader() { return NEWS_LOADER; }
}
