package de.cmassive.vplanx.rest.rest;

import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.lib.exception.InvalidCredentialException;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.info.AuthInfo;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import de.cmassive.vplanx.rest.controller.SchoolUserSubscriptionRestController;
import de.cmassive.vplanx.rest.loader.DocumentConverter;
import de.cmassive.vplanx.rest.model.feature.VpxFeature;
import de.cmassive.vplanx.rest.model.feature.VpxFlag;
import de.cmassive.vplanx.rest.model.school.VpxSchool;
import de.cmassive.vplanx.rest.model.user.VpxSubscription;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import org.bson.Document;

public class RestCommon {

    public static boolean validateUserIdent(String ueif) {
        BasicDBObject object = new BasicDBObject("accountUID", ueif);
        return RestApplication.getMongoConnection().getAccountCollection().find(object).first() != null;
    }

    public static VpxUser validateUserIdentRet(String ueif) {
        BasicDBObject object = new BasicDBObject("accountUID", ueif);
        Document doc = RestApplication.getMongoConnection().getAccountCollection().find(object).first();
        if(doc == null) return null;

        return DocumentConverter.userFromDocument(doc);
    }

    public static boolean hasUserValidSubscription(VpxSchool school, VpxUser user, String ftName) {
        VpxSubscription subscription = user.getSubscription(school.getSchoolId(), ftName);
        return hasUserValidSubscription(school, subscription);
    }

    public static boolean hasUserValidSubscription(VpxSchool school, VpxSubscription userSubscription) {
        if(userSubscription == null) return false;
        VpxFeature schoolFeature = school.getFeature(userSubscription.getFeatureName());

        if(schoolFeature == null) return false;
        if(schoolFeature.getAuthContainer() == null || schoolFeature.getAuthContainer().getAuthInfo() == null) {
            System.out.println("AUTH OF SCHOOL NULL");
            return true;
        } else if(userSubscription.getAuthContainer() == null || userSubscription.getAuthContainer().getAuthInfo() == null) {
            return false;
        }

        AuthContainer userContainer = userSubscription.getAuthContainer();
        AuthContainer schoolContainer = schoolFeature.getAuthContainer();

        if(!userContainer.getAuthType().equalsIgnoreCase(schoolContainer.getAuthType())) return false;

        VpxFlag authSourceFlag = schoolFeature.getFlag(VpxFlag.FLAG_AUTH_SOURCE);
        if(authSourceFlag != null && authSourceFlag.getValue().equalsIgnoreCase("dynamic")) {
            System.out.println("IS DYNAMIC");
            return true;
        }

        System.out.println("BEFORE CHECK OF " + userContainer.getAuthInfo().toRawData() + " -> " + schoolContainer.getAuthInfo().toRawData());
        return userContainer.getAuthInfo().equals(schoolContainer.getAuthInfo());
    }

    public static VpxSubscription subscribeUserToFeature(VpxSchool school, VpxUser user, SchoolUserSubscriptionRestController.VpxSubscriptionBean bean) throws InvalidCredentialException {
        {
            VpxSubscription subscription = user.getSubscription(school.getSchoolId(), bean.getFeatureName());
            if(subscription != null) {
                System.out.println("return valid subscription");
                return subscription;
            }
        }

        VpxFeature feature = school.getFeature(bean.getFeatureName());
        if(feature == null) return null;
        VpxSubscription subscription = null;

        System.out.println("FT FOUND");

        if(feature.getAuthContainer() == null || feature.getAuthContainer().getAuthInfo() == null) {
            subscription = new VpxSubscription(RestApplication.getMongoConnection().getNextSequenceId("subscriptions") , school.getSchoolId(), user.getAccountId(), bean.getFeatureName());
            subscribeUserToFeatureDatabase(subscription, false);
            return subscription;
        }

        VpxFlag dynamicFlag = feature.getFlag(VpxFlag.FLAG_AUTH_SOURCE);
        if(dynamicFlag != null && dynamicFlag.getValue().equalsIgnoreCase("dynamic")) {
            //dynamic auth not supported at the moment (TODO: quick check)
            return null;
        } else { //static auth
            AuthInfo info = null;
            String schoolAuthType = feature.getAuthContainer().getAuthType();
            if(schoolAuthType.equalsIgnoreCase("auth-static")) {
                info = new UserPasswordAuth();
                if(!info.withRawData(bean.getData())) {
                    System.out.println("NO RAW INIT");
                    return null;
                }
            }

            System.out.println("BEFORE INFO CHECK");
            if(info == null) return null;

            if(info.equals(feature.getAuthContainer().getAuthInfo())) {
                subscription = new VpxSubscription(RestApplication.getMongoConnection().getNextSequenceId("subscriptions"), school.getSchoolId(), user.getAccountId(), feature.getName());

                AuthContainer container = new AuthContainer();
                container.setAuthInfo(info);
                container.setAuthType(schoolAuthType);
                subscription.setAuthContainer(container);
                subscribeUserToFeatureDatabase(subscription, true);
            } else {
                throw new InvalidCredentialException(null);
            }

            return subscription;
        }
    }

    public static boolean unsubscribeUserFromFeature(VpxSubscription subscription) {
        return unsubscribeUserFromFeatureDatabase(subscription);
    }

    private static boolean unsubscribeUserFromFeatureDatabase(VpxSubscription subscription) {
        BasicDBObject query = new BasicDBObject("accountId", subscription.getAccountId());
        Document found = RestApplication.getMongoConnection().getAccountCollection().find(query).first();
        if(found == null) return false;

        Document pull = new Document();
        pull.append("schoolId", subscription.getSchoolId());
        pull.append("feature", subscription.getFeatureName());

        BasicDBObject pullList = new BasicDBObject("subscriptions", pull);
        RestApplication.getMongoConnection().getAccountCollection().updateOne(found, new BasicDBObject("$pull", pullList));
        return true;
    }

    private static void subscribeUserToFeatureDatabase(VpxSubscription subscription, boolean pushAuth) {
        BasicDBObject query = new BasicDBObject("accountId", subscription.getAccountId());
        Document found = RestApplication.getMongoConnection().getAccountCollection().find(query).first();
        if(found == null) return;
        AuthContainer container = subscription.getAuthContainer();

        Document insert = new Document();
        insert.append("subscriptionId", subscription.getSubscriptionId());
        insert.append("schoolId", subscription.getSchoolId());
        insert.append("feature", subscription.getFeatureName());

        if(container != null && pushAuth) {
            insert.append("auth", new Document().append("type", container.getAuthType()).append("data", container.getAuthInfo().toRawData()));
        }

        BasicDBObject insertObject = new BasicDBObject("subscriptions", insert);
        RestApplication.getMongoConnection().getAccountCollection().updateOne(found, new BasicDBObject("$push", insertObject));
    }
}
