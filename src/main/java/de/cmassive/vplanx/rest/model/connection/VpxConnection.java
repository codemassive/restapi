package de.cmassive.vplanx.rest.model.connection;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class VpxConnection {

    private MongoClient client;
    private MongoDatabase vpxDatabase;

    private MongoCollection<Document> accountCollection;
    private MongoCollection<Document> schoolCollection;
    private MongoCollection<Document> sequenceCollection;

    private MongoCollection<Document> substitutionCollection;
    private MongoCollection<Document> schoolFeatureCollection;
    private MongoCollection<Document> newsCollection;

    public VpxConnection(String address, int port) {
        client = new MongoClient(address, port);
        vpxDatabase = client.getDatabase("planomy");

        accountCollection = vpxDatabase.getCollection("accounts");
        schoolCollection = vpxDatabase.getCollection("schools");
        sequenceCollection = vpxDatabase.getCollection("sequences");
        substitutionCollection = vpxDatabase.getCollection("ft-substitution");
        schoolFeatureCollection = vpxDatabase.getCollection("school-ft");
        newsCollection = vpxDatabase.getCollection("ft-news");
    }

    public MongoCollection<Document> getAccountCollection() {
        return accountCollection;
    }

    public MongoCollection<Document> getSchoolCollection() {
        return schoolCollection;
    }

    public MongoCollection<Document> getSequenceCollection() {
        return sequenceCollection;
    }

    public MongoCollection<Document> getSubstitutionCollection() {
        return substitutionCollection;
    }

    public MongoCollection<Document> getSchoolFeatureCollection() {
        return schoolFeatureCollection;
    }

    public MongoCollection<Document> getNewsCollection() {
        return newsCollection;
    }

    public synchronized int getNextSequenceId(String seqName) {
        BasicDBObject query = new BasicDBObject("seqName", seqName);
        Document doc = this.sequenceCollection.find(query).first();

        if(doc == null) return -1;

        int currId = doc.getInteger("seqId");
        BasicDBObject insert = new BasicDBObject("seqId", currId+1);
        this.sequenceCollection.updateOne(query, new BasicDBObject("$set", insert));

        return currId;
    }

    public synchronized int peekNextSequenceId(String seqName) {
        BasicDBObject query = new BasicDBObject("seqName", seqName);
        Document doc = this.sequenceCollection.find(query).first();

        if(doc == null) return -1;

        return doc.getInteger("seqId")+1;
    }

    public Document getSchoolById(int id) {
        BasicDBObject object = new BasicDBObject("schoolId", id);
        return schoolCollection.find(object).first();
    }
}
