package de.cmassive.vplanx.rest.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;

public class VpxSubscription {

    @JsonIgnore
    private int accountId;

    @JsonIgnore
    private AuthContainer authContainer;

    private int schoolId;
    private int subscriptionId;
    private String featureName;

    public VpxSubscription(int subscriptionId, int schoolId, int accountId, String featureName) {
        this.schoolId = schoolId;
        this.accountId = accountId;
        this.featureName = featureName;
        this.subscriptionId = subscriptionId;
    }

    public VpxSubscription() {

    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public void setAuthContainer(AuthContainer authContainer) {
        this.authContainer = authContainer;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public int getAccountId() {
        return accountId;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public String getFeatureName() {
        return featureName;
    }

    public AuthContainer getAuthContainer() {
        return authContainer;
    }

    @JsonProperty("authDataAvailable")
    public boolean isCommon() {
        return this.authContainer != null;
    }
}
