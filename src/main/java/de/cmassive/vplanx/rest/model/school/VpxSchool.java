package de.cmassive.vplanx.rest.model.school;


import com.fasterxml.jackson.annotation.JsonProperty;
import de.cmassive.vplanx.rest.model.feature.VpxFeature;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class VpxSchool {

    //GENERAL INFO
    private int schoolId;
    private Set<VpxFeature> features = new HashSet<>();

    //BASIC INFO
    @JsonProperty("info")
    private VpxInfo schoolInfo;

    @JsonProperty("address")
    private VpxAddress schoolAddress;

    @JsonProperty("contact")
    private VpxContact schoolContact;

    private List<String> schoolClasses;

    public VpxSchool() {

    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public Set<VpxFeature> getFeatures() {
        return features;
    }

    public void setFeatures(Set<VpxFeature> features) {
        this.features = features;
    }

    public VpxFeature getFeature(String ftName) {
        for(VpxFeature f : this.features) {
            if(f.getName().equalsIgnoreCase(ftName)) {
                return f;
            }
        }
        return null;
    }

    public VpxInfo getSchoolInfo() {
        return schoolInfo;
    }

    public void setSchoolInfo(VpxInfo schoolInfo) {
        this.schoolInfo = schoolInfo;
    }

    public VpxAddress getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(VpxAddress schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public VpxContact getSchoolContact() {
        return schoolContact;
    }

    public void setSchoolContact(VpxContact schoolContact) {
        this.schoolContact = schoolContact;
    }

    public List<String> getSchoolClasses() {
        return schoolClasses;
    }

    public void setSchoolClasses(List<String> schoolClasses) {
        this.schoolClasses = schoolClasses;
    }
}
