package de.cmassive.vplanx.rest.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class VpxUser {

    @JsonIgnore
    private int accountId;

    @JsonProperty("userId")
    private String exposedId;

    @JsonProperty("subscriptions")
    private HashMap<Integer, ArrayList<VpxSubscription>> subscriptionsMap = new HashMap<>();

    public VpxUser(int accountId, String exposedId) {
        this.accountId = accountId;
        this.exposedId = exposedId;
    }

    public int getAccountId() {
        return accountId;
    }

    public String getExposedId() {
        return exposedId;
    }

    public HashMap<Integer, ArrayList<VpxSubscription>> getSubscriptions() {
        return subscriptionsMap;
    }

    public VpxSubscription getSubscription(int schoolId, String ftName) {
        ArrayList<VpxSubscription> subscriptions = this.subscriptionsMap.get(schoolId);
        if(subscriptions != null) {
            for(VpxSubscription subscription : subscriptions) {
                if(subscription.getFeatureName().equalsIgnoreCase(ftName)) {
                    return subscription;
                }
            }
        }

        return null;
    }

    public VpxSubscription getSubscription(int subId) {
        for(ArrayList<VpxSubscription> subscriptions : this.subscriptionsMap.values()) {
            for(VpxSubscription subscription : subscriptions) {
                if(subscription.getSubscriptionId() == subId) {
                    return subscription;
                }
            }
        }

        return null;
    }

    public ArrayList<VpxSubscription> getSubscriptions(int schoolId) {
        return this.subscriptionsMap.getOrDefault(schoolId, new ArrayList<>());
    }

    @JsonProperty("subscriptions")
    public ArrayList<VpxSubscription> getSubscriptionsCombined() {
        ArrayList<VpxSubscription> combined = new ArrayList<>();
        for(ArrayList<VpxSubscription> f : this.subscriptionsMap.values()) {
            combined.addAll(f);
        }

        return combined;
    }

    public void addSubscription(VpxSubscription subscription) {
        ArrayList<VpxSubscription> subscriptions = this.subscriptionsMap.getOrDefault(subscription.getSchoolId(), new ArrayList<>());
        subscriptions.add(subscription);

        if(!this.subscriptionsMap.containsKey(subscription.getSchoolId())) {
            this.subscriptionsMap.put(subscription.getSchoolId(), subscriptions);
        }
    }
}
