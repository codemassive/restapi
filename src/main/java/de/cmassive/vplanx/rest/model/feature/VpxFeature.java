package de.cmassive.vplanx.rest.model.feature;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.Endpoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VpxFeature {

    private String name;

    @JsonIgnore
    private HashMap<String, String> additionalData = new HashMap<>();

    @JsonIgnore
    private List<VpxFlag> flags = new ArrayList<>();

    @JsonIgnore
    private AuthContainer authContainer;

    private List<Endpoint> endpoints = new ArrayList<>();
    boolean authRequired = false;

    public VpxFeature() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(HashMap<String, String> additionalData) {
        this.additionalData = additionalData;
    }

    public List<VpxFlag> getFlags() {
        return flags;
    }

    public void setFlags(List<VpxFlag> flags) {
        this.flags = flags;
    }

    public List<Endpoint> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(List<Endpoint> endpoints) {
        this.endpoints = endpoints;
    }

    public AuthContainer getAuthContainer() {
        return authContainer;
    }

    public void setAuthContainer(AuthContainer authContainer) {
        this.authContainer = authContainer;
    }

    @JsonProperty("authRequired")
    public boolean isAuthRequired() {
        return authContainer != null || (getFlag(VpxFlag.FLAG_AUTH_SOURCE) != null && getFlag(VpxFlag.FLAG_AUTH_SOURCE).getValue().equalsIgnoreCase("dynamic"));
    }

    public void setAuthRequired(boolean authRequired) {
        this.authRequired = authRequired;
    }

    public VpxFlag getFlag(String name) {
        for(VpxFlag flag : this.flags) {
            if(flag.getName().equalsIgnoreCase(name)) return flag;
        }

        return null;
    }
}
