package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.rest.model.news.News;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import de.cmassive.vplanx.rest.rest.RestCommon;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping("/schools/{schoolId}/news")
@RestController
public class SchoolNewsRestController {

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ArrayList<News>> getSchoolNews(@PathVariable int schoolId, @RequestHeader("VPX_USER_IDENT") String userIdent) {
        VpxUser user = RestCommon.validateUserIdentRet(userIdent);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }

        if (!RestCommon.hasUserValidSubscription(
                RestApplication.getSchoolLoader().getSchoolById(schoolId),
                user,
                "ft-news")) {
            return ResponseEntity.status(org.springframework.http.HttpStatus.FORBIDDEN).build();
        }

        return ResponseEntity.ok(RestApplication.getNewsLoader().getNewsBySchool(schoolId));
    }
}
