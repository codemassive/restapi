package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.lib.exception.InvalidCredentialException;
import de.cmassive.vplanx.rest.model.school.VpxSchool;
import de.cmassive.vplanx.rest.model.user.VpxSubscription;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import de.cmassive.vplanx.rest.rest.RestCommon;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/user/subscriptions")
public class SchoolUserSubscriptionRestController {

    public static class VpxSubscriptionBean {

        private int schoolId;
        private String featureName;
        private HashMap<String, String> data = new HashMap<>();

        public VpxSubscriptionBean() {

        }

        public HashMap<String, String> getData() {
            return data;
        }

        public void setData(HashMap<String, String> data) {
            this.data = data;
        }

        public int getSchoolId() {
            return schoolId;
        }

        public void setSchoolId(int schoolId) {
            this.schoolId = schoolId;
        }

        public String getFeatureName() {
            return featureName;
        }

        public void setFeatureName(String featureName) {
            this.featureName = featureName;
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> readSubscriptions(@RequestHeader("VPX_USER_IDENT") String ident) {
        VpxUser user = null;
        if((user = RestCommon.validateUserIdentRet(ident)) == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(user.getSubscriptionsCombined());
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> createSubscription(@RequestHeader("VPX_USER_IDENT") String ident, @RequestBody VpxSubscriptionBean bean) {
        VpxUser user = null;
        if((user = RestCommon.validateUserIdentRet(ident)) == null) {
            return ResponseEntity.notFound().build();
        }

        VpxSchool school = RestApplication.getSchoolLoader().getSchoolById(bean.getSchoolId());
        if(school == null) return ResponseEntity.notFound().build();

        try {
            System.out.println("LOL");
            VpxSubscription subscription = RestCommon.subscribeUserToFeature(school, user, bean);
            if(subscription == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

            return ResponseEntity.ok(subscription);
        } catch (InvalidCredentialException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{subId}")
    ResponseEntity<?> querySubscription(@RequestHeader("VPX_USER_IDENT") String ident, @PathVariable Integer subId) {
        VpxUser user = null;
        if((user = RestCommon.validateUserIdentRet(ident)) == null) {
            return ResponseEntity.notFound().build();
        }

        VpxSubscription subscription = user.getSubscription(subId);
        if(subscription == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(subscription);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{subId}")
    ResponseEntity<?> deleteSubscription(@RequestHeader("VPX_USER_IDENT") String ident, @PathVariable Integer subId) {
        VpxUser user = null;
        if((user = RestCommon.validateUserIdentRet(ident)) == null) {
            return ResponseEntity.notFound().build();
        }

        VpxSubscription subscription = user.getSubscription(subId);
        if(subscription == null) {
            return ResponseEntity.notFound().build();
        }

        if(!RestCommon.unsubscribeUserFromFeature(subscription)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok().build();
    }
}
