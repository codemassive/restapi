package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.bson.Document;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.UUID;

@RestController
@RequestMapping("/register")
public class RegisterUserRestController {

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> registerNewUser(@RequestHeader HttpHeaders headers) {
        if(headers.containsKey("VPX_USER_IDENT")) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("VPX_USER_IDENT set!");
        }

        int nextUserId = RestApplication.getMongoConnection().getNextSequenceId("users");
        UUID exposedId = UUID.randomUUID();

        Document document = new Document();
        document.append("accountId", nextUserId);
        document.append("accountUID", exposedId.toString());
        document.append("subscriptions", new ArrayList<Object>());

        RestApplication.getMongoConnection().getAccountCollection().insertOne(document);
        return ResponseEntity.ok(RestApplication.getUserLoader().getUserById(exposedId.toString()));
    }

}
