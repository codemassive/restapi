package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class SchoolUserRestController {

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> readUser(@RequestHeader("VPX_USER_IDENT") String userExpId) {
        if(userExpId == null || userExpId.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        VpxUser user = RestApplication.getUserLoader().getUserById(userExpId);
        if(user == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(user);
    }
}
