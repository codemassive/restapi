package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.rest.model.school.VpxSchool;
import de.cmassive.vplanx.rest.model.substitution.VpxSubstitution;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;

@RestController
@RequestMapping("/schools")
public class SchoolRestController {

    /*
    Returns all schools
     */
    @RequestMapping(method = RequestMethod.GET)
    Collection<VpxSchool> allSchools() {
        return RestApplication.getSchoolLoader().getAllSchools();
    }

    /*
    Returns a specific school by id
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{schoolId}")
    ResponseEntity<?> readSchool(@PathVariable Integer schoolId) {
        try {
            VpxSchool school = RestApplication.getSchoolLoader().getSchoolById(schoolId);
            return ResponseEntity.ok(school);
        }catch(Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }


}
