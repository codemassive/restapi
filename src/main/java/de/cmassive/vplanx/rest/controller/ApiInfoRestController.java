package de.cmassive.vplanx.rest.controller;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.cmassive.vplanx.rest.rest.RestApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiInfoRestController {

    private static ObjectNode API_INFO_NODE;

    static {
        API_INFO_NODE = JsonNodeFactory.instance.objectNode();
        API_INFO_NODE.put("apiLevel", RestApplication.API_LEVEL);
        API_INFO_NODE.put("name", RestApplication.API_NAME);
        API_INFO_NODE.put("company", RestApplication.API_COMPANY);
        API_INFO_NODE.put("developers", RestApplication.API_DEVELOPERS);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ObjectNode getApiInfo() {
        return API_INFO_NODE;
    }

}
