package de.cmassive.vplanx.rest.controller;

import de.cmassive.vplanx.rest.model.substitution.VpxSubstitution;
import de.cmassive.vplanx.rest.model.user.VpxUser;
import de.cmassive.vplanx.rest.rest.RestApplication;
import de.cmassive.vplanx.rest.rest.RestCommon;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/schools/{schoolId}/substitutions")
public class SchoolSubstitutionRestController {

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> readSubstitutions(
            @PathVariable int schoolId,
            @RequestHeader("VPX_USER_IDENT") String userIdent,
            @RequestParam(value = "class", required = false) String cls) {
        VpxUser user = RestCommon.validateUserIdentRet(userIdent);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }

        if(!RestCommon.hasUserValidSubscription(RestApplication.getSchoolLoader().getSchoolById(schoolId), user, "ft-substitution")) {
            return ResponseEntity.status(org.springframework.http.HttpStatus.FORBIDDEN).build();
        }

        if(cls != null && !cls.isEmpty()) {
            return ResponseEntity.ok(RestApplication.getSubLoader().getSubDaysBySchoolUser(schoolId, user, cls));
        }

        return ResponseEntity.ok(RestApplication.getSubLoader().getSubDaysBySchoolUser(schoolId, user));
    }
}
